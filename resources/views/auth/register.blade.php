<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <title>Up Everything You Want!</title>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style_mobile.css')}}">
</head>

<body>
    <div class="container">
        <div class="row center" style="margin-top: 70px">
            <div class="col l6 offset-l3 s12">
                <a href="{{route('welcome')}}">
                    <img src="{{asset('images/logo_showup_color.png')}}" alt="" style="max-width: 250px; max-height: 235px">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col l10 offset-l1 s12">
                <p style="font-size: 22px; font-weight: bold">Buat akun Show Up!</p>
                <p>
                    Sudah punya akun Show Up! ?
                    <a href="{{route('login')}}" class="accent-text">
                        Masuk
                    </a>
                </p>

                <form action="{{route('register')}}" style="margin-top: 30px" method="post">
                  {{ csrf_field() }}
                    <div class="row">
                        <div class="input-field col s12 l5 primary-input">
                            <i class="material-icons prefix primary-text" style="margin-top: 10px">account_circle</i>
                            <input name="name" placeholder="Masukan Nama Anda" id="name" type="text" class="validate" required>
                            <label for="name" style="margin-bottom: 10px">Nama Lengkap</label>
                        </div>
                        <div class="input-field col s12 l5 primary-input right">
                            <i class="material-icons prefix primary-text" style="margin-top: 10px">photo_camera</i>
                            <input name="instagram_account" placeholder="Masukan Instagram Anda" id="instagram" type="text" class="validate" required>
                            <label for="instagram" style="margin-bottom: 10px">Instagram</label>
                        </div>
                        <div class="input-field col s12 l5 primary-input">
                            <i class="material-icons prefix primary-text" style="margin-top: 10px">email</i>
                            <input name="email" placeholder="Masukan Email Anda" id="email" type="email" class="validate" required>
                            <label for="email" style="margin-bottom: 10px">Email</label>
                        </div>
                        <div class="input-field col s12 l5 primary-input right">
                            <i class="material-icons prefix primary-text" style="margin-top: 10px">lock</i>
                            <input name="password" placeholder="Masukan Password Anda" id="password" type="password" class="validate" required>
                            <label for="password" style="margin-bottom: 10px">Sandi</label>
                        </div>
                        <p class="center">
                            <label>
                                <input type="checkbox" required/>
                                <span>Saya setuju dengan ketentuan dan layanan</span>
                            </label>
                        </p>
                        <div class="input-field col s12 l6 offset-l3 center">
                            <button type="submit" class="btn primary-color full-width"><b>DAFTAR</b></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--JavaScript at end of body for optimized loading-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="{{asset('js/custom.js')}}"></script>
</body>

</html>
