@extends('layouts.app')
@section('content')
  <div class="container" style="margin-top: 60px">
        <!-- START ABOUT -->
        <div class="row">
            <div class="col l12 s12">
                <h3><b>Tentang Show Up!</b></h3>
                <p>
                    Show Up! Adalah suatu startup yang fokus pada dunia periklanan yang berdiri pada 23 Agustus 2017 dan didirikan oleh
                    Taufan Fadhilah Iskandar, Niken Febriani Kusumawati sebagai, Muhammad Yusuf Matra. Tujuan didirikannya Show Up! Sebagai
                    wadah dimana para pengusaha yang ingin memasarkan produknya secara efektif dan efisien dan juga suatu penyelenggara
                    acara yang ingin menyebarkan posternya secara viral kepada publik melalui perantara dunia maya. Beberapa pihak yang
                    bekerja sama dengan kita adalah para pengusaha, pengguna Instagram, penyelenggara acara, dan figur publik. Dengan
                    adanya wadah di Show Up! dapat menciptakan tujuan yang baik dari berbagai pihak.
                </p>
            </div>
        </div>
        <!-- END ABOUT -->
        <div class="row">
            <!-- START VISION -->
            <div class="col l6">
                <h3><b>Visi Kami</b></h3>
                <p>
                    Menjadikan Show Up! perusahaan periklanan terbesar di dunia melalui bantuan sosial media secara efektif dan efisien
                </p>
            </div>
            <!-- END VISION -->
            <!-- START MISSION -->
            <div class="col l6">
                <h3><b>Misi Kami</b></h3>
                <ul>
                    <li>
                        Selalu mengetahui teknologi terbaik
                    </li>
                    <li>
                        Selalu mengikuti trend sosial media
                    </li>
                    <li>
                        Memberikan pelayanan terbaik
                    </li>
                    <li>
                        Memberikan solusi terbaik bagi semua aspek yang ingin mengiklankan produk nya dan menyebarluaskan informasi.
                    </li>
                    <li>
                        Menyediakan strategi untuk meningkatkan penjualan suatu brand menggunakan Show Up!
                    </li>
                </ul>
            </div>
            <!-- END MISSION -->
        </div>
        <div class="row center">
            <!-- START TEAM -->
            <div class="col l12">
                <h3><b>Tim Kami</b></h3>
            </div>
            <div class="col l3 s12">
                <img src="{{asset('images/img_taufan.png')}}" style="max-width: 200px">
                <br>
                <b style="min-height: 500px;">Taufan Fadhilah Iskandar</b>
                <p>Chief Executive Officer</p>
            </div>
            <div class="col l3 s12">
                <img src="{{asset('images/img_niken.png')}}" style="max-width: 200px">
                <br>
                <b style="min-height: 500px;">Niken Febriani K.</b>
                <p>Chief Marketing Officer</p>
            </div>
            <div class="col l3 s12">
                <img src="{{asset('images/img_khrisna.png')}}" style="max-width: 200px">
                <br>
                <b style="min-height: 500px;">Anantya Khrisna Seta</b>
                <p>Chief Operation Officer</p>
            </div>
            <div class="col l3 s12">
                <img src="{{asset('images/img_yusuf.png')}}" style="max-width: 200px">
                <br>
                <b style="min-height: 500px;">Muhammad Yusuf Matra</b>
                <p>Chief Creative Officer</p>
            </div>
            <!-- END TEAM -->
        </div>
    </div>
@endsection
