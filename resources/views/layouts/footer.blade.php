<!-- FOOTER START -->
    <footer class="page-footer" style="background-color: #041a2b">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <img src="{{asset('images/logo_showup_color.png')}}" style="max-width: 150px; margin-top: 20px">
                    <p class="grey-text text-lighten-4">
                        Jl. H. Umar RT/RW 06/13 Kav. 22
                        <br>
                        Daeyuhkolot, Bandung
                        <br>
                        40256
                    </p>
                </div>
                <div class="col l3 s12">
                    <h5 class="white-text">Temui Kami</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="mailto:public.relation@showup.id" target="_top">Email</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://www.instagram.com/showup.official/?hl=id">Instagram</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/show.up.1650">Facebook</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">LINE</a></li>
                    </ul>
                </div>
                <div class="col l3 s12">
                    <h5 class="white-text">Kontak</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="mailto:public.relation@showup.id">public.relation@showup.id</a></li>
                        <li><a class="grey-text text-lighten-3" href="tel:+628112326009">+6281 1232 6009</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                Copyright © 2017 Show Up!
                <!-- <a class="grey-text text-lighten-4 right" href="#!">More Links</a> -->
            </div>
        </div>
    </footer>
    <!-- FOOTER END -->
