<!-- START NAVBAR -->
    <nav>
        <div class="nav-wrapper primary-color">
            <a href="{{route('welcome')}}" class="brand-logo">
                <img src="{{asset('images/logo-showup.png')}}" class="hide-on-med-and-up navbar-logo-mobile">
                <img src="{{asset('images/logo-showup.png')}}" class="show-on-medium-and-up hide-on-med-and-down navbar-logo">
            </a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="left hide-on-med-and-down" style="margin-left: 250px">
                <li><a href="{{route('advertisement')}}" style="color: #febb00">Daftar Iklan</a></li>
                <li><a href="{{route('influencer')}}">Daftar Influencer</a></li>
                <li><a href="{{route('about')}}">Tentang Kami</a></li>
                <li><a href="{{route('faq')}}">FAQ</a></li>
            </ul>
            <ul class="right hide-on-med-and-down">
              @if (isset(Auth::user()->id))
                <a class='dropdown-trigger' href='{{route('welcome')}}' data-target='dropdown2' style="margin-top: 10px; margin-right: 50px">
                    <img src="{{asset('storage/'.Auth::user()->avatar)}}" class="show-on-med-and-up" style="max-height: 50px;">
                </a>
                @else
                  <a class='dropdown-trigger' href='{{route('welcome')}}' data-target='dropdown1' style="margin-top: 10px; margin-right: 50px">
                      <img src="{{asset('images/ic_user.png')}}" class="show-on-med-and-up" style="max-height: 50px;">
                  </a>
              @endif
                <ul id='dropdown1' class='dropdown-content'>
                    <li><a href="{{route('login')}}" class="primary-text">Masuk</a></li>
                    <li><a href="{{route('register')}}">Daftar</a></li>
                    <li class="divider" tabindex="-1"></li>
                </ul>
                <ul id='dropdown2' class='dropdown-content'>
                    <li><a class="primary-text" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">Logout</a></li>
                    <li class="divider" tabindex="-1"></li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </ul>
        </div>
    </nav>

    <ul class="sidenav" id="mobile-demo">
        <li><a href="{{route('advertisement')}}">Daftar Iklan</a></li>
        <li><a href="{{route('influencer')}}">Daftar Influencer</a></li>
        <li><a href="{{route('about')}}">Tentang Kami</a></li>
        <li><a href="{{route('faq')}}">FAQ</a></li>
        <li><a href="login.html">Login</a></li>
    </ul>
    <!-- END NAVBAR -->
