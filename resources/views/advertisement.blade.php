@extends('layouts.app')
@section('content')
  <!-- START HEADER -->
   <div class="container-fluid primary-color">
       <div class="row">
           <div class="col l10 offset-l1 white-text center">
               <h5><b>Temukan Iklan yang cocok dan promosikan pada akun Sosial Media anda</b></h5>
               <p>Kami bekerja sama dengan banyak pelaku Bisnis yang memasarkan produk mereka</p>
           </div>
       </div>
       <div class="row">
           <div class="col l8 offset-l2 s12 white" style="border-radius: 30px; border: solid 1.5px #bebebe; margin-bottom: 20px">
               <form action="">
                   <div class="input-field col l10 s10">
                       <input placeholder="Cari iklan yang cocok..." type="text" class="validate">
                   </div>
                   <div class="input-field col l2 s2">
                       <button class="btn white full-width center" style="color: #9c9c9c;">
                           <i class="material-icons">search</i>
                       </button>
                   </div>
               </form>
           </div>
       </div>
   </div>
   <!-- END HEADER -->

   <!-- START ADV -->
   <div class="container-fluid" style="margin-left: 30px; margin-right: 30px">
       @for ($i=0; $i < 10; $i++)
         <div class="row adv">
             <a href="{{route('advertisementDetail')}}" class="black-text">
               <div class="col l4">
                   <img src="{{asset('images/img_pemilik_produk.png')}}" class="adv-img">
               </div>
               <div class="col l8">
                   <h5><b>Lorem Ipsum Dummy Text</b></h5>
                   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet Lorem
                   ipsum dolor sit amet, consectetuer adipiscing elit, sed diam…</p>
                   <p style="font-size: 12px">Penawar : <b>2 Orang</b></p>
                   <p style="font-size: 12px">Durasi : <b>3 Hari</b></p>
                   <div class="row valign-wrapper">
                       <div class="col l1">
                           <img src="{{asset('images/img_khrisna.png')}}" class="adv-ava-img">
                       </div>
                       <div class="col l11">
                           <p>Muhammad Yusuf Matra</p>
                       </div>
                   </div>
               </div>
             </a>
         </div>
       @endfor
       <!-- START PAGINATION -->
       <div class="row">
           <div class="col l12 center">
               <ul class="pagination">
                   <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                   <li class="active"><a href="#!">1</a></li>
                   <li class="waves-effect"><a href="#!">2</a></li>
                   <li class="waves-effect"><a href="#!">3</a></li>
                   <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
               </ul>
           </div>
       </div>
       <!-- END PAGINATION -->
   </div>
   <!-- END ADV -->
@endsection
