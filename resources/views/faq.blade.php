@extends('layouts.app')
@section('content')
  <div class="container" style="margin-top: 60px">
        <div class="row" style="border-radius: 30px; border: solid 1.5px #bebebe;">
            <form action="">
                <div class="input-field col l10">
                    <input placeholder="Butuh Bantuan ?" type="text" class="validate">
                </div>
                <div class="input-field col l2">
                    <button class="btn accent-color full-width">CARI</button>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col l12">
                <h5><b>Apa itu Show Up! ?</b></h5>
                <p style="margin-bottom: 30px">Show Up! adalah suatu platform yang menghubungkan pemilik produk yang ingin mengiklankan produknya dengan para pengguna
                sosial media agar dapat meningkatkan profit secara maksimal dengan baik dan memberikan penghasilan bagi pengguna sosial
                media</p>
                <hr>
            </div>
            <div class="col l12">
                <h5><b>Bagaimana Show Up bekerja?</b></h5>
                <p style="margin-bottom: 30px">
                    Cara kerja Show Up! <br>
                    <ol>
                        <li>
                            Pemilik produk menambahkan spesifikasi iklan mereka pada <a href="http://www.showup.id">showup.id</a>
                        </li>
                        <li>
                            Instagram influencer mengajukan penawaran terhadap iklan
                        </li>
                        <li>
                            Pemilik produk memilih Instagram influencer yang sesuai dengan kebutuhan
                        </li>
                        <li>
                            Pemilik produk melakukan transaksi berupa pembayaran jasa iklan dengan <a href="http://www.showup.id">showup.id</a>
                        </li>
                        <li>
                            Instagram influencer mengunggah iklan sesuai ketentuan yang telah disepakati
                        </li>
                        <li>
                            <a href="http://www.showup.id">showup.id</a> meneruskan transaksi ke Instagram influencer
                        </li>
                    </ol>
                </p>
                <hr>
            </div>
            <div class="col l12">
                <h5><b>Bagaimana cara untuk bergabung menjadi pengiklan ?</b></h5>
                <p style="margin-bottom: 30px">
                    Anda dapat mendaftarkan diri pada untaian berikut <a href="http://showup.id/register">http://showup.id/register</a> secara gratis
                </p>
                <hr>
            </div>
            <div class="col l12">
                <h5><b>Bagaimana cara untuk bergabung menjadi influencer ?</b></h5>
                <p style="margin-bottom: 30px">
                    Anda dapat mendaftarkan diri pada untaian berikut <a href="http://showup.id/register">http://showup.id/register</a>
                    secara gratis
                </p>
                <hr>
            </div>
            <div class="col l12">
                <h5><b>Apa manfaat memasang iklan di Show Up ?</b></h5>
                <p style="margin-bottom: 30px">
                    Jika Anda sebagai pemilik produk atau pemasang iklan, Anda akan mendapatkan hal berikut jika bergabung dengan kami:
                    <ol>
                        <li>
                            Kemudahan pemasaran produk
                        </li>
                        <li>
                            Brand Produk lebih terkenal
                        </li>
                        <li>
                            Kenaikan keuntungan
                        </li>
                    </ol>
                </p>
                <hr>
            </div>
            <div class="col l12">
                <h5><b>Apa manfaat menjadi influencer di Show Up! ?</b></h5>
                <p style="margin-bottom: 30px">
                    Jika Anda sebagai influencer atau pengiklan, Anda akan mendapatkan hal berikut jika bergabung dengan kami:
                    <ol>
                        <li>
                            Memperoleh penghasilan tambahan
                        </li>
                        <li>
                            Peningkatan pengikut Instagram
                        </li>
                        <li>
                            Menambah popularitas
                        </li>
                    </ol>
                </p>
                <hr>
            </div>
            <div class="col l12">
                <h5><b>Bagaiman skema bagi hasil jika bekerjasama dengan Show Up! ?</b></h5>
                <p style="margin-bottom: 30px">
                    Skema bagi hasil keuntungan pada Show Up! yaitu setiap transaksi akan dikenakan potongan administrasi sebesar 10%.
                </p>
                <hr>
            </div>
            <div class="col l12">
                <h5><b>Bagaimanakah prosedur dan persyaratan menjadi mitra pengiklan di Show Up! ?</b></h5>
                <p style="margin-bottom: 30px">
                    Tidak ada prosedur khusus untuk menjadi mitra pengiklan kami. Siapa saja yang bersedia menjadi mitra kami siap
                    menerima.
                </p>
                <hr>
            </div>
            <div class="col l12">
                <h5><b>Bagaimana cara menghubungi Show Up! jika ingin tahu lebih lanjut tentang kerjasama yang dilakukan?</b></h5>
                <p style="margin-bottom: 30px">
                    Narahubung kami selalu siap sedia membantu Anda mulai dari pukul 8.00 WIB hingga 17.00 WIB.
                    <br>
                    Daftar Kontak kami:
                    <ul>
                        <li><a href="mailto:public.relation@showup.id" target="_top">Email</a></li>
                        <li><a href="https://www.instagram.com/showup.official/?hl=id">Instagram</a></li>
                        <li><a href="https://www.facebook.com/show.up.1650">Facebook</a></li>
                        <li><a href="#!">LINE</a></li>
                    </ul>
                </p>
                <hr>
            </div>
        </div>
    </div>
@endsection
