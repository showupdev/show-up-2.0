@extends('layouts.app')
@section('content')
  <!-- START ADV CONTENT -->
    <div class="container" style="margin-top: 30px">
        <div class="row">
            <div class="col l4 s12">
                <img src="{{asset('images/photo1.png')}}" class="responsive-img adv-detail-img">
                <div class="row">
                    <div class="col l3 s3">
                        <img src="{{asset('images/photo1.png')}}" class="responsive-img adv-detail-img">
                    </div>
                    <div class="col l3 s3">
                        <img src="{{asset('images/photo1.png')}}" class="responsive-img adv-detail-img">
                    </div>
                    <div class="col l3 s3">
                        <img src="{{asset('images/photo1.png')}}" class="responsive-img adv-detail-img">
                    </div>
                    <div class="col l3 s3">
                        <img src="{{asset('images/photo1.png')}}" class="responsive-img adv-detail-img">
                    </div>
                </div>
            </div>
            <div class="col l8 s12">
                <!-- START STATUS -->
                <div class="row" style="border-radius: 3.5px; background-color: #041a2b;">
                    <div class="col l1 s2">
                        <img src="{{asset('images/alarm.png')}}" alt="" style="max-width: 28px; margin-top: 40%">
                    </div>
                    <div class="col l9 s10">
                        <p class="white-text">Anda memiliki durasi 3 hari untuk memasang iklan</p>
                    </div>
                    <div class="col l2 s12">
                        <p class="accent-text right">Berjalan</p>
                    </div>
                </div>
                <!-- END STATUS -->
                <div class="row">
                    <!-- START ADV -->
                    <div class="col l8 s12">
                        <b>Judul</b>
                        <p>Lorem Ipsum Dummy Text</p>

                        <b>Caption Untuk Instagram</b>
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet Lorem
                            ipsum dolor sit amet, text dum consectetuer adipiscing elit, sed diam…
                        </p>

                        <b>Tarif per Penawar</b>
                        <p>
                            Rp. 5.0000
                        </p>
                        <div class="row">
                            <div class="col l6">
                                <p>
                                    2018-04-27 01:29:24
                                </p>
                            </div>
                            <div class="col l6">
                                <button class="btn primary-color">AJUKAN PENAWARAN</button>
                            </div>
                        </div>
                    </div>
                    <!-- END ADV -->
                    <!-- START PROFILE -->
                    <div class="col l4 s12 center" style="padding: 30px; border-radius: 3.5px; border: solid 0.5px #bebebe; background-color: #ffffff;">
                        <b>Pengiklan</b> <br>
                        <img src="{{asset('images/img_taufan.png')}}" alt="" class="circle responsive-img" style="max-height: 100px">
                        <br>
                        <b>Muhammad Yusuf Matra</b>
                    </div>
                    <!-- END PROFILE -->
                </div>
                <div class="row">
                    <div class="col 12">
                        <h6>Daftar Penawar</h6>
                    </div>
                </div>
                @for ($i=0; $i < 5; $i++)
                  <div class="row adv-bid-list">
                      <div class="col l2">
                          <img src="{{asset('images/img_taufan.png')}}" alt="" class="circle responsive-img" style="max-height: 40px; margin-top: 10%">
                      </div>
                      <div class="col l10">
                          <b>Taufan Fadhilah Iskandar</b>
                          <p>Instagram <a href="">@taufanfadhilah</a></p>
                      </div>
                  </div>
                @endfor
            </div>
        </div>
    </div>
    <!-- END ADV CONTENT -->

    <div class="container hide-on-med-and-down">
        <div class="row">
            <div class="col l12">
                <h5>Rekomendasi untuk Anda</h5>
                <hr>
            </div>
            @for ($i=0; $i < 4; $i++)
              <div class="col l3 s12">
                  <div class="card">
                      <div class="card-image">
                          <img src="{{asset('images/photo1.png')}}">
                      </div>
                      <div class="card-content">
                          <b>Lorem Ipsum Dummy Text for graphic designer…</b>
                          <hr>
                          <div class="row">
                              <div class="col l3">
                                  <img src="{{asset('images/img_taufan.png')}}" alt="" class="circle" style="max-height: 40px; margin-top: 10%">
                              </div>
                              <div class="col l6">
                                  <b>Muhammad Yusuf</b>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col l12">
                                  <b>Total Penawar</b>
                                  <p>20 Orang</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            @endfor
        </div>
    </div>
@endsection
