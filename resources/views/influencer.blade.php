@extends('layouts.app')
@section('content')
  <!-- START HEADER -->
   <div class="container-fluid primary-color">
       <div class="row">
           <div class="col l10 offset-l1 white-text center">
               <h5><b>Temukan Influencer yang cocok dan promosikan pada akun Sosial Media anda</b></h5>
               <p>Kami bekerja sama dengan banyak Influencer Instagram yang memasarkan produk mereka</p>
           </div>
       </div>
       <div class="row">
           <div class="col l8 offset-l2 s12 white" style="border-radius: 30px; border: solid 1.5px #bebebe; margin-bottom: 20px">
               <form action="">
                   <div class="input-field col l10 s10">
                       <input placeholder="Cari Influencer terbaikmu..." type="text" class="validate">
                   </div>
                   <div class="input-field col l2 s2">
                       <button class="btn white full-width center" style="color: #9c9c9c;">
                           <i class="material-icons">search</i>
                       </button>
                   </div>
               </form>
           </div>
       </div>
   </div>
   <!-- END HEADER -->
  <!-- START INFLUENCER -->
    <div class="container-fluid" style="margin-left: 50px; margin-right: 50px">
        <div class="row center">
            @for ($i=0; $i < 12; $i++)
              <div class="col l3">
                  <div class="card" style="border-radius: 7.5px; border: solid 0.5px #bebebe;">
                      <div class="card-content">
                          <img src="{{asset('images/img_taufan.png')}}" class="influencer-img">
                          <br>
                          <b>Muhammad Yusuf Matra</b>
                          <br>
                          <a href="#" class="accent-text">@yusufmatra</a>
                          <br>
                          <p class="accent2-text">Bandung, Indonesia</p>
                          <br>
                          <div class="row" style="text-align: center">
                              <div class="col l6">
                                  <p style="font-size: 12px">
                                      Followers :
                                      <br>
                                      <b>155,23</b>
                                  </p>
                              </div>
                              <div class="col l6">
                                  <p style="font-size: 12px">
                                      Iklan :
                                      <br>
                                      <b>150,17</b>
                                  </p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            @endfor
        </div>
        <!-- START PAGINATION -->
        <div class="row">
            <div class="col l12 center">
                <ul class="pagination">
                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="active"><a href="#!">1</a></li>
                    <li class="waves-effect"><a href="#!">2</a></li>
                    <li class="waves-effect"><a href="#!">3</a></li>
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                </ul>
            </div>
        </div>
        <!-- END PAGINATION -->
    </div>
    <!-- END INFLUENCER -->
@endsection
