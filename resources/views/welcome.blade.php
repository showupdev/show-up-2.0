@extends('layouts.app')
@section('content')
  <!-- HEADER START -->
    <div class="container-fluid" style="margin-top: 60px;min-height: 600px; background-image: url({{asset('images/bg-header.png')}}); background-size: cover">
        <div class="row">
            <div class="col l4 offset-l1" style="margin-top: 50px">
                <h3><b>CARI, TEMUKAN,</b></h3>
                <h5>PILIH SEGALA YANG KAMU INGINKAN</h5>
                <p>
                    Promosikan produk Anda atau jadilah promotor melalui sosial media Anda
                </p>
                <a href="{{route('register')}}">
                    <button class="btn full-width primary-color" style="margin-bottom: 25px; margin-top: 20px">
                        DAFTAR
                    </button>
                </a>

                <a href="{{route('login')}}">
                    <button class="btn full-width primary-text white">
                        MASUK
                    </button>
                </a>
            </div>
        </div>
    </div>
    <!-- END END -->

    <!-- USER START -->
    <div class="container center" style="margin-bottom: 110px">
        <div class="row" style="margin-top: 100px; margin-bottom: 35px">
            <div class="col-l12">
                <h3><b>Pengguna Show Up!</b></h3>
            </div>
        </div>
        <div class="row">
            <div class="col l3 s6">
                <img src="{{asset('images/ic-bisnis-baru.png')}}" class="ic_user">
                <p>
                    UMKM Baru
                </p>
            </div>
            <div class="col l3 s6">
                <img src="{{asset('images/ic_instagram.png')}}" class="ic_user">
                <p>
                    Pengguna Instagram
                </p>
            </div>
            <div class="col l3 s6">
                <img src="{{asset('images/ic_event_oranizer.png')}}" class="ic_user">
                <p>
                    Penyelenggara Acara
                </p>
            </div>
            <div class="col l3 s6">
                <img src="{{asset('images/ic_influenser.png')}}" class="ic_user">
                <p>
                    Figur Publik
                </p>
            </div>
        </div>
    </div>
    <!-- USER END -->

    <!-- BENEFIT START -->
    <div class="container">
        <div class="row">
            <div class="col l12 center show-on-medium-and-up hide-on-med-and-down">
                <h3 ><b>Keuntungan Bergabung di Show Up!</b></h3>
            </div>
            <div class="col l12 center hide-on-med-and-up">
                <h4><b>Keuntungan Bergabung di Show Up!</b></h4>
            </div>
        </div>
        <div class="row">
            <div class="col l6">
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-image">
                                <img src="{{asset('images/img_pemilik_produk.png')}}">
                            </div>
                            <div class="card-content">
                                <h5><b>Pemilik Produk</b></h5>
                                <ul>
                                    <li style="margin-bottom: 10px">Kemudahan pemasaran produk</li>
                                    <li style="margin-bottom: 10px">Brand Produk lebih terkenal</li>
                                    <li style="margin-bottom: 10px">Kenaikan keuntungan</li>
                                </ul>
                                <button class="btn white primary-text full-width" style="margin-top: 20px">
                                    PASARKAN PRODUK ANDA
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col l6">
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-image">
                                <img src="{{asset('images/img_pengguna_instagram.png')}}">
                            </div>
                            <div class="card-content">
                                <h5><b>Pengguna Instagram</b></h5>
                                <ul>
                                    <li style="margin-bottom: 10px">Memperoleh penghasilan tambahan</li>
                                    <li style="margin-bottom: 10px">Peningkatan pengikut Instagram</li>
                                    <li style="margin-bottom: 10px">Menambah popularitas</li>
                                </ul>
                                <button class="btn white primary-text full-width" style="margin-top: 20px">
                                    LIHAT PENAWARAN
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BENEFIT END -->

    <!-- HOW WORK START -->
    <div class="container-fluid primary-color" style="min-height: 600px">
        <div class="row">
            <div class="col l12 white-text center">
                <h3><b>Cara Kerja Show Up!</b></h3>
            </div>
        </div>
        <div class="row">
            <div class="col s6 l4">
                <div class="card" style="min-height: 300px">
                    <div class="card-content">
                        <p style="width: 27.6px; height: 27.6px; background-color: #bebebe; border-radius: 50px; text-align: center; padding-top: 5px; color: white">1</p>
                        <p style="margin-top: 30px">Pemilik produk menambahkan spesifikasi iklan mereka pada <a href="http://wwww.showup.id">showup.id</a></p>
                    </div>
                </div>
            </div>
            <div class="col s6 l4">
                <div class="card" style="min-height: 300px">
                    <div class="card-content">
                        <p style="width: 27.6px; height: 27.6px; background-color: #bebebe; border-radius: 50px; text-align: center; padding-top: 5px; color: white">2</p>
                        <p style="margin-top: 30px">Instagram influencer mengajukan penawaran terhadap iklan</p>
                    </div>
                </div>
            </div>
            <div class="col s6 l4">
                <div class="card" style="min-height: 300px">
                    <div class="card-content">
                        <p style="width: 27.6px; height: 27.6px; background-color: #bebebe; border-radius: 50px; text-align: center; padding-top: 5px; color: white">3</p>
                        <p style="margin-top: 30px">Pemilik produk memilih Instagram influencer yang sesuai dengan kebutuhan</p>
                    </div>
                </div>
            </div>
            <div class="col s6 l4">
                <div class="card" style="min-height: 300px">
                    <div class="card-content">
                        <p style="width: 27.6px; height: 27.6px; background-color: #bebebe; border-radius: 50px; text-align: center; padding-top: 5px; color: white">4</p>
                        <p style="margin-top: 30px">Pemilik produk melakukan transaksi berupa pembayaran jasa iklan dengan <a href="http://www.showup.id">showup.id</a></p>
                    </div>
                </div>
            </div>
            <div class="col s6 l4">
                <div class="card" style="min-height: 300px">
                    <div class="card-content">
                        <p style="width: 27.6px; height: 27.6px; background-color: #bebebe; border-radius: 50px; text-align: center; padding-top: 5px; color: white">5</p>
                        <p style="margin-top: 30px">Instagram influencer mengunggah iklan sesuai ketentuan yang telah disepakati</p>
                    </div>
                </div>
            </div>
            <div class="col s6 l4">
                <div class="card" style="min-height: 300px">
                    <div class="card-content">
                        <p style="width: 27.6px; height: 27.6px; background-color: #bebebe; border-radius: 50px; text-align: center; padding-top: 5px; color: white">6</p>
                        <p style="margin-top: 30px"><a href="http://www.showup.id">showup.id</a> meneruskan transaksi ke Instagram influencer</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HOW WORK END -->

    <!-- STATISTIC START -->
    <div class="container">
        <div class="row">
            <div class="col l12 center">
                <h3><b>Statistik Show Up!</b></h3>
            </div>
        </div>
        <div class="row center">
            <div class="col l3 s6">
                <h4 class="primary-text">500</h4>
                <p>Jumlah Iklan</p>
            </div>
            <div class="col l3 s6">
                <h4 class="primary-text">130</h4>
                <p>Jumlah Influencer</p>
            </div>
            <div class="col l3 s6">
                <h4 class="primary-text">250</h4>
                <p>Jumlah Pengikut</p>
            </div>
            <div class="col l3 s6">
                <h4 class="primary-text">0</h4>
                <p>Jumlah Figur Publik</p>
            </div>
        </div>
        <div class="row" style="margin-top: 60px; margin-bottom: 60px">
            <div class="col l6 offset-l3">
                <a href="{{route('register')}}">
                    <button class="btn primary-color white-text full-width">
                        <b>Saya ingin bergabung dengan Show Up!</b>
                    </button>
                </a>
            </div>
        </div>
    </div>
    <!-- STATISTIC END -->
@endsection
