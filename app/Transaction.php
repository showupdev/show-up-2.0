<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['advertisement_id', 'total', 'status', 'confirmation_photo', 'unique_code'];
}
