<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $fillable = ['adverstisement_id', 'user_id',' price', 'status'];
}
