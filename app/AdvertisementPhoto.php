<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementPhoto extends Model
{
    protected $fillable = ['advertisement_id', 'photo'];
}
