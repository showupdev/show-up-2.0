<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('about', function() {
  return view('about');
})->name('about');

Route::get('faq', function() {
  return view('faq');
})->name('faq');

Route::get('advertisement', function() {
  return view('advertisement');
})->name('advertisement');

Route::get('advertisementDetail', function() {
  return view('advertisementDetail');
})->name('advertisementDetail');

Route::get('influencer', function() {
  return view('influencer');
})->name('influencer');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
