<?php

use Illuminate\Database\Seeder;
use App\Advertisement;
class AdvertisementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Advertisement::create([
          'user_id' => 2,
          'title' => 'Show Up!',
          'description' => 'Show Up! adalah portal yang memiliki misi menghubungkan antara yang menawarkan barang/jasa dalam bentuk iklan ke pada yang mencari barang/jasa tersebut. Barang dapat berupa produk yang ingin jual mereka, jasa yang ingin mereka tawarkan atupun bisa sebuah perusahaan yang sedang membuka lowongan pekerjaan dapat juga memasangkan lowongan tersebut di Show Up!.',
          'status' => 'waiting'
        ]);
    }
}
