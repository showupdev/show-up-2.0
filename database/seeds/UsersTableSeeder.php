<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          'name' => 'Admin',
          'email' => 'admin@showup.id',
          'password' => bcrypt('qwe123'),
          'instagram_account' => 'showup.office',
          'instagram_follower' => 500,
          'city' => 'Bandung',
          'avatar' => 'avatar/default.png',
          'level' => 'Admin'
        ]);

        User::create([
          'name' => 'Taufan Fadhilah Iskandar',
          'email' => 'taufan@showup.id',
          'password' => bcrypt('qwe123'),
          'instagram_account' => 'taufanfadhilah',
          'instagram_follower' => 435,
          'city' => 'Malang',
          'avatar' => 'avatar/default.png',
          'level' => 'User'
        ]);

        User::create([
          'name' => 'Niken Febriani Kusumawati',
          'email' => 'niken@showup.id',
          'password' => bcrypt('qwe123'),
          'instagram_account' => 'nikenfebrianik',
          'instagram_follower' => 760,
          'city' => 'Demak',
          'avatar' => 'avatar/default.png',
          'level' => 'User'
        ]);
    }
}
