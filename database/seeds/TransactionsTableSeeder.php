<?php

use Illuminate\Database\Seeder;
use App\Transaction;
class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::create([
          'advertisement_id' => 1,
          'total' => 40000,
          'unique_code' => 554,
          'status' => 'unpaid',
          'confirmation_photo' => 'confirmation/default.png'
        ]);
    }
}
