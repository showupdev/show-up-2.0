<?php

use Illuminate\Database\Seeder;
use App\Bid;
class BidsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bid::create([
          'advertisement_id' => 1,
          'user_id' => 3,
          'price' => 40000,
          'status' => 'ongoing'
        ]);
    }
}
