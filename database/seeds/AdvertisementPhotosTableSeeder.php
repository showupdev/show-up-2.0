<?php

use Illuminate\Database\Seeder;
use App\AdvertisementPhoto;
class AdvertisementPhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdvertisementPhoto::create([
          'advertisement_id' => 1,
          'photo' => 'advertisement/default.png'
        ]);
        AdvertisementPhoto::create([
          'advertisement_id' => 1,
          'photo' => 'advertisement/example.png'
        ]);
    }
}
