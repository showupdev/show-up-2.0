<?php

use Illuminate\Database\Seeder;
use App\Transfer;
class TransfersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transfer::create([
          'user_id' => 3,
          'total' => 36000,
          'confirmation_photo' => 'confirmation_bid/default.png'
        ]);
    }
}
